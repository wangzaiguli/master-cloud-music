manster云音乐
(问问题有偿，文档已经介绍的很清楚了，产生问题多百度，细看说明文档)
#### 介绍
基于网易云音乐真实接口开发的音乐小程序

#### 软件架构
Nodejs作为后端，跨站请求伪造 (CSRF), 伪造请求头 , 调用官方 API

[网易云音乐NodeJS版API](https://binaryify.github.io/NeteaseCloudMusicApi/#/)


使用ES6语法

使用promise对象进行异步请求资源

使用moment.js进行时间日期处理

使用pubsub.js消息的发布订阅，进行组件间数据的传递

安装详情请看 [https://blog.csdn.net/qq_45803593/article/details/125421471](https://blog.csdn.net/qq_45803593/article/details/125421471)

歌词相关请看 [https://blog.csdn.net/qq_45803593/article/details/114064285](https://blog.csdn.net/qq_45803593/article/details/114064285)

#### 预览效果如下
##### 初始页面
![初始页面](https://images.gitee.com/uploads/images/2021/0214/110613_9eb39fdf_8531883.png "index.png")

##### 登录页面
![登录页面](https://images.gitee.com/uploads/images/2021/0214/110706_6051c091_8531883.png "login.png")

##### 个人中心页面
![个人中心页面](https://images.gitee.com/uploads/images/2021/0214/110724_ae52c98a_8531883.png "person.png")

##### 推荐歌曲页面
![推荐歌曲页面](https://images.gitee.com/uploads/images/2021/0214/110744_31fffee1_8531883.png "recommendSong.png")

##### 视频页面
![视频页面](https://images.gitee.com/uploads/images/2021/0214/110804_ccf6d227_8531883.png "video.png")

##### 热搜页面
![热搜页面](https://images.gitee.com/uploads/images/2021/0214/110822_6447cbe3_8531883.png "hotSearch.png")

##### 搜索列表
![搜索列表](https://images.gitee.com/uploads/images/2021/0214/110851_b73a082e_8531883.png "searchList.png")

##### 历史搜索记录
![历史搜索记录](https://images.gitee.com/uploads/images/2021/0228/101514_0d59160a_8531883.png "5@O3N5_BR]AIQV_04N2FTGV.png")

##### 歌曲详情页面
![歌曲详情页面](https://images.gitee.com/uploads/images/2021/0225/103707_2af1320a_8531883.png "7XTTM23O@K989}3D67M2ZRS.png")